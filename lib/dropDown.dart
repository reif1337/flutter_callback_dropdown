import 'package:flutter/material.dart';

const Map<String, String> testMap = {
  "Menu 1": 'Sigi',
  "Menu 2": 'Franzi',
  "Menu 3": 'Fritzi',
  "Menu 4": 'Werni',
  "Menu 5": 'Sandi',
  "Menu 6": 'Tobi',
  "Menu 7": 'Mausi',
  "Menu 8": 'Maxi',
  "Menu 9": 'Basti',
  "Menu 0": 'Susi',
};

class MyDropDown extends StatefulWidget {
  const MyDropDown({super.key});

  @override
  State<MyDropDown> createState() => _MyDropDownState();
}

class _MyDropDownState extends State<MyDropDown> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  var dropdownValue = testMap.keys.toList().first;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          DropdownButton<String>(
            isExpanded: true,
            value: dropdownValue,
            items: testMap.keys.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text("--> $value"),
              );
            }).toList(),
            onChanged: (String? value) async {
              setState(() {
                dropdownValue = value!;
              });
            },
          ),
          Text("$dropdownValue --- ${testMap[dropdownValue]}")
        ],
      ),
    );
  }
}
