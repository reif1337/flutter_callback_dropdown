import 'dart:math';

import 'package:flutter/material.dart';

class MyCallback extends StatefulWidget {
  const MyCallback({super.key});

  @override
  State<MyCallback> createState() => _MyCallbackState();
}

class _MyCallbackState extends State<MyCallback> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const SizedBox(
            height: 10, //<-- SEE HERE
          ),
          TestTile(removeMe),
        ],
      ),
    );
  }

  removeMe(int a) {
    setState(() {
      print("wuhuuu $a");
    });
  }
}


class TestTile extends StatefulWidget {
  Function myTestFunction;

  TestTile(this.myTestFunction, {super.key});

  @override
  State<TestTile> createState() => _TestTileState();
}

class _TestTileState extends State<TestTile> {
  int a = 5;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text("Test $a" ),
      trailing: IconButton(
        icon: Icon(Icons.ac_unit),
        onPressed: () {
          a = Random().nextInt(8);
          print("----  $a");
          widget.myTestFunction(a);
        },
      ),
    );
  }
}
